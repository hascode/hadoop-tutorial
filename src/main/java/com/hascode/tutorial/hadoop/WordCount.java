package com.hascode.tutorial.hadoop;

import java.io.IOException;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class WordCount extends Configuration implements Tool {
	public static void main(final String[] args) throws Exception {
		int result = ToolRunner.run(new Configuration(), new WordCount(), args);
		System.exit(result);
	}

	public Configuration getConf() {
		return new Configuration();
	}

	public void setConf(final Configuration conf) {
	}

	public static class MapperClass extends MapReduceBase implements
			Mapper<LongWritable, Text, Text, IntWritable> {

		public void map(final LongWritable key, final Text value,
				final OutputCollector<Text, IntWritable> output,
				final Reporter reporter) throws IOException {
			String line = value.toString();
			StringTokenizer tok = new StringTokenizer(line);
			while (tok.hasMoreTokens()) {
				output.collect(new Text(tok.nextToken().getBytes()),
						new IntWritable(1));
			}
		}
	}

	public static class ReducerClass extends MapReduceBase implements
			Reducer<Text, IntWritable, Text, IntWritable> {

		public void reduce(final Text key, final Iterator<IntWritable> values,
				final OutputCollector<Text, IntWritable> output,
				final Reporter reporter) throws IOException {
			int total = 0;
			while (values.hasNext())
				total += values.next().get();
			output.collect(key, new IntWritable(total));
		}
	}

	public int run(final String[] args) throws Exception {
		Configuration conf = getConf();
		JobConf job = new JobConf(conf, WordCount.class);
		job.setJobName("GeneticsMapReduceJob");
		job.setMapperClass(MapperClass.class);
		job.setReducerClass(ReducerClass.class);
		job.setOutputFormat(TextOutputFormat.class);
		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(IntWritable.class);
		JobClient.runJob(job);
		return 0;
	}

}
